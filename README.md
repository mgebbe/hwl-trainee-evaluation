# HWL trainee evaluation

This project offers different kind of frameworks and tasks to solve with them. Each task can be solved independently, so that a different knowledge base can be tested for each trainee.

The topic of the application revolves around gaming. It automatically starts an H2 database on startup and fills it with test data.

## Possible Tasks

A rough outline of tasks a trainee may solve

### Spring

- create a service
- create a scheduled task
- ...

### Spring Web

- create a *REST GET* endpoint
- create a *REST POST* endpoint
- ...

### Spring Data / JPA

- get specific data from a repository
- create a new repository
- create a new dto
- custom table/column mapping
- ...

### Junit / Mockito

- write tests
- use specific functionality of *JUnit 5*
- use *Mockito* for verification of method calls
- use *Mockito* for mocks
- ...

### Maven

- configure pom
- execute goals
- add goals
- ...

## List of concrete Tasks

- get all games from a specific year via REST
  - add an optional `min rating` as a parameter
- implement a repository query for games with a rating of at least 6
- implement a scheduled task in the `GameCleaner` class which removes all games older than 5 years from the database every 5 minutes
- explain how the `dependencyManagement` works and what it does in this specific project
- implement a unit test for the `setRating()` method of the `Game` dto which tests all valid numbers
  - add a second test which tests at least two invalid numbers
- implement a single unit test method for the `setName()` method of the `Game` dto which tests (mostly) all invalid input strings
- implement a unit test to verify the order of `toString()` calls on `Game` dtos for the `printRanking` method of the `GameRankingUtil` class
  - also verify the amount of calls on each dto
- explain the use case for `mockito-inline` and how it is configured
- explain why the dependency `mockito-core` is not mentioned