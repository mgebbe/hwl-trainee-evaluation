package com.hellmann.games.task;

import com.hellmann.games.repository.GameRepository;
import com.hellmann.games.util.GameRankingUtil;
import org.springframework.stereotype.Component;

@Component
public class GameCleaner {

    private final GameRepository repository;

    public GameCleaner(GameRepository gameRepository) {
        this.repository = gameRepository;
    }

    public void printRankedGames() {
        GameRankingUtil.printRanking(repository.findAll());
    }
}
