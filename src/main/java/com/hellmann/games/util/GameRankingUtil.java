package com.hellmann.games.util;

import com.hellmann.games.dto.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public final class GameRankingUtil {

    private static final Logger logger = LoggerFactory.getLogger(GameRankingUtil.class);

    public static void printRanking(List<Game> games) {
        for (Game game : sortByRanking(games)) {
            logger.info(game.toString());
        }

    }

    private static List<Game> sortByRanking(List<Game> games) {
        games.sort((game1, game2) -> {
            if(game1.getRating() == game2.getRating()){
                    return game1.getName().compareTo(game2.getName());
            }
            return game1.getRating() > game2.getRating() ? -1 : 1;
        });

        return games;
    }

}
