package com.hellmann.games.dto;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity(name = "GAME")
@Immutable
public class Game {

    @Id
    private String name;
    private String publisher;
    private int yearOfRelease;
    private int rating;

    public Game() {}

    public Game(
            String name,
            String publisher,
            int yearOfRelease,
            int rating
    ) {
        setName(name);
        setPublisher(publisher);
        setYearOfRelease(yearOfRelease);
        setRating(rating);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null && !name.isBlank())
            this.name = name;
        else
            throw new IllegalArgumentException("invalid name");
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        if (publisher != null && !publisher.isBlank())
            this.publisher = publisher;
        else
            throw new IllegalArgumentException("invalid publisher");
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        if (1950 < yearOfRelease && yearOfRelease < LocalDate.now().getYear() + 5)
            this.yearOfRelease = yearOfRelease;
        else
            throw new IllegalArgumentException("invalid year of release");
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        if (0 < rating && rating < 11)
            this.rating = rating;
        else
            throw new IllegalArgumentException("invalid rating");
    }

    @Override
    public String toString() {
        return name + " (" + publisher + ", " + yearOfRelease + "): " + rating + "/10";
    }
}
