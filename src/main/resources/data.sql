INSERT INTO GAME (NAME, PUBLISHER, YEAR_OF_RELEASE, RATING) VALUES
('Borderlands 2', '2K Games', 2012, 10),
('Cities:Skylines', 'Paradox Interactive', 2015, 8),
('Counter-Strike: Global Offense', 'Valve', 2012, 7),
('Deep Rock Galactic', 'Coffee Stain Studios', 2020, 9),
('Doom', 'Bethesda', 2016, 10),
('Farmville', 'Zynga', 2009, 3),
('Maneater', 'Tripwire Interactive', 2020, 5),
('Stardew Valley', 'Chucklefish', 2016, 9),
('The Stanley Parable', 'Galactic Cafe', 2013, 6),
('WarCraft III: Reforged', 'Blizzard Entertainment', 2020, 1);
